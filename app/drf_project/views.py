from django.http import JsonResponse


def ping(request):
    data = {"ping": "deployed with CI/CD"}
    return JsonResponse(data)
