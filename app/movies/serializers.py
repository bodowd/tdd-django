from rest_framework import serializers

from .models import Movie


# ModelSerializer outputs all the fields from the model
class MovieSerializer(serializers.ModelSerializer):
    # ModelSerializer includes default implementations
    # of .create() and .update()
    class Meta:
        model = Movie
        fields = "__all__"
        # ensure these fields are never created or updated via the serializer
        read_only_fields = ("id", "created_date", "updated_date")
