import pytest

from movies.models import Movie


# fixture that uses the factory as a fixture pattern to add a few movies before some tests
@pytest.fixture(scope="function")
def add_movie():
    def _add_movie(title, genre, year):
        # directly use the model to add to db
        movie = Movie.objects.create(title=title, genre=genre, year=year)
        return movie

    return _add_movie
