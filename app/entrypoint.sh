#!/bin/sh

# since the movies service is dependent not only on the container being up and running but also the actual Postgres
# instance being up and healthy, use this to make sure the postgres is up then run python manage...

if [ "$DATABASE" = "postgres"]
then
    echo "Waiting for postgres..."
    
    # SQL_HOST defines the the name of the service movies-db in .env.dev
    while ! nc -z $SQL_HOST $SQL_PORT; do
        sleep 0.1
    done

    echo "PostgreSQL started"
fi

python manage.py flush --no-input
python manage.py migrate

exec "$@"